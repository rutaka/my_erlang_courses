-module(test).
-export([init/0, filter_female/1, get_names/1, split_by_age/2, get_femail_names/1, get_stat/1]).

init() ->
  [{user, 1, "Bob", male, 27},
   {user, 2, "Helen", female, 18},
   {user, 3, "Bill", male, 33},
   {user, 4, "Kate", female, 18}].

filter_female(Users) ->
  filter_female(Users, []).

filter_female([], Acc) -> lists:reverse(Acc);
filter_female([{user, _, _, Gender, _} | Rest], Acc) when Gender =:= male ->
      filter_female(Rest, Acc);
filter_female([{user, _, _, Gender, _} = User | Rest], Acc) when Gender =:= female ->
      filter_female(Rest, [User | Acc]).

get_names(Users) ->
  get_names(Users, []).

get_names([], Acc) -> Acc;
get_names([User | Rest], Acc) ->
  {user, _, Name, _, _} = User,
  get_names(Rest, [Name | Acc]).

get_femail_names(Users) ->
  F = fun({user, _Id, Name, Gender, _Age}) ->
          case Gender of
            male -> false;
            female -> {true, Name}
          end
      end,
  lists:filtermap(F, Users).

get_stat(Users) ->
  Stat0 = {0, 0, 0, 0.0},
  F = fun(User, Acc) ->
          {user, _, _, Gender, Age} = User,
          {TotalUser, TotalMale, TotalFemail, TotalAge} = Acc,
          {TotalMale2, TotalFemail2} =
          case Gender of
            male -> {TotalMale + 1, TotalFemail};
            female -> {TotalMale, TotalFemail + 1}
          end,
          {TotalUser + 1, TotalMale2, TotalFemail2, TotalAge + Age}
      end,
  {TU, TM, TF, TA} = lists:foldl(F, Stat0, Users),
  {TU, TM, TF, TA / TU}.

split_by_age(Users, Age) ->
  Acc = {[],[]},
  F = fun(User, {M, F}) ->
          {user, _, _, _, UserAge} = User,
          if
            UserAge > Age ->
              {[User | M], F};
            true ->
              {M, [User | F]}
          end
      end,
  lists:foldl(F, Acc, Users).


