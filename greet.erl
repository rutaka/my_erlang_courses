-module(greet).
-export([greet/2,msum/1]).
-author("rutaka").

greet(male, Name) ->
  io:format("Hello, Mr. ~s!", [Name]);

greet(famale, Name) ->
  io:format("Hello, Mrs. ~s!", [Name]);

greet(_, Name) ->
  io:format("Hello, ~s!", [Name]).

msum([]) -> 0;
msum([H|T]) -> H + msum(T).
