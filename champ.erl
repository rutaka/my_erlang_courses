-module(champ).

-export([init/0, check_strong/1, check_count_player/1]).

init() ->
  [
    {team, "Cool Bools", [
      {player, "Bill", 35},
      {player, "Bob", 24},
      {player, "BB", 15},
      {player, "TT", 40}]},
    {team, "Good Gays", [
      {player, "dBill", 11},
      {player, "dBob", 22},
      {player, "dBB", 33},
      {player, "dTT", 44}]},
    {team, "Crazy Frog", [
      {player, "cBill", 10},
      {player, "cBob", 8},
      {player, "cBB", 5},
      {player, "cTT", 4}]},
    {team, "Mighty Dogs", [
      {player, "eBill", 40},
      {player, "eBob", 50},
      {player, "eBB", 65},
      {player, "eTT", 55}]}
  ].

check_strong(Teams) ->
    F = fun({team, Name, Players}) ->
            {team, Name, [Player || {player, _PName, Strong} = Player <- Players, Strong > 10]}
        end,
    lists:map(F, Teams).

check_count_player(Teams) ->
    [Team || {team, _Name, Players} = Team <- Teams, length(Players) > 2].
