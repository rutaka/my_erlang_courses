-module(my_gen_server).
-export([start/0, server/1, add_user/2, remove_user/2, get_users/1, stop/1, get_users_count/1, crash/1]).

start() ->
    io:format("starting server~n"),
    InitialState = [],
    spawn(?MODULE, server, [InitialState]).

add_user(Pid, User) ->
    Pid ! {add_user, User}.

remove_user(Pid, User) ->
    Pid ! {remove_user, User}.

get_users_count(Pid) ->
    call(Pid, get_users_count).

get_users(Pid) ->
    call(Pid, get_users).

crash(Pid) ->
    call(Pid, crash).

call(Pid, Msg) ->
    Monitor = erlang:monitor(process, Pid),
    Pid ! {Msg, self(), Monitor},
    receive
      {reply, Monitor, Reply} ->
        erlang:demonitor(Monitor, [flush]),
        Reply;
      {'DOWN', Monitor, _, _, ErrorReason} ->
        {error, ErrorReason}
    after 500 ->
        erlang:demonitor(Monitor, [flush]),
        no_reply
    end.

stop(Pid) ->
    Pid ! stop.

server(State) ->
    io:format("ver3 ~p My state is ~p~n", [self(), State]),
    receive
      {add_user, User} -> NewState = [User | State],
                  ?MODULE:server(NewState);
      {remove_user, User} ->
          NewState = lists:delete(User, State),
                  ?MODULE:server(NewState);
      {get_users, Pid, Ref} -> Pid ! {reply, Ref, State},
                  ?MODULE:server(State);
      {get_users_count, Pid, Ref} -> Pid ! {reply, Ref, length(State)},
                  ?MODULE:server(State);
      {crash, _From, _Ref} -> io:format("Iam going to crash :(~n"),
                500 / 0,
                ?MODULE:server(State);
      stop -> io:format("server ~p stopping~n", [self()]);
      Msg -> io:format("server ~p got message ~p~n", [self(), Msg]),
                  ?MODULE:server(State)
    end.
